#!/usr/bin/bash

function fail {
	echo "$1" >/dev/stderr
	exit 1
}

hash rsync || fail "Dependencies not met"

here="$(realpath "$(dirname "$0")")"

# directory with the world
world="$here/world"
[ -d "$world" ] || fail "World directory not found: $world"

# directory where all the backups go to
bakdir="$here/backups"
mkdir -p "$bakdir"

# clean up empty backup directories
rmdir --ignore-fail-on-non-empty "$bakdir"/*

# check if there already is a backup
baklast="$(ls -1 "$bakdir"|tail -1)"

# make directory for the current backup
now="$(date -Is)"
bak="$bakdir/$now"
mkdir "$bak" || fail "Failed creating backup target directory: $bak"

# logfile
# must not be in $bakdir, else the last log file will be chosen for --link-dest
logdir="$here/logs"
mkdir -p "$logdir"
logfile="$logdir/backup_$now"


# $baklast must be an absolute path, else it's treated as relative to the
# destination directory.  We made sure that $baklast is an absolute path when
# we set $here above.
[ -n "$baklast" ] && args="--link-dest='$bakdir/$baklast/'"

# The slashes at the end of each rsync path argument are needed, else the world
# directory will be copied to $destination/world instead of $destination/
args="$args -avH '$world/' '$bak/'"

# run the backup
cmd="rsync $args"
(echo "Running $cmd";echo;eval "$cmd") |& tee "$logfile"
