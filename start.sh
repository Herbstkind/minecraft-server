#!/usr/bin/bash

function error {
	echo "$1"
	exit 1
}

# Check that we're the user we want to be
user=minecraft
whoami | grep -qo "$user" || error "Only user $user is allowed to start the server!"

# Check that server is not already running
ps -e | grep -q java && error "Minecraft seems to be running already, found a Java process"

while :
do
	# logging
	# We want a new log file each time, so that it's visible at a glance,
	# how many restarts there were.
	scriptdir="$(dirname "$0")"
	logfile="$scriptdir/$(date -Is)"
	echo "Logging to $logfile"

	# We don't need the log4j argument anymore since 1.18.1, but it can't
	# hurt to leave it in. In case we need to roll back to 1.17.1 or
	# something.
	java -Xmx3000M -Xms2000M -Dlog4j2.formatMsgNoLookups=true -jar server.jar nogui |& tee "$logfile"

	n=5
	echo "Server restarts in $n seconds... (Ctrl+c to abort)"
	sleep $n
done
